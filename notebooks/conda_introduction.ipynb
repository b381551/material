{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a5340efb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Conda - a python package and environment manager\n",
    "\n",
    "![conda_logo](../images/conda_logo.png)\n",
    "\n",
    "from https://conda.io/projects/conda/en/latest/index.html\n",
    "\n",
    "> Conda is an open-source package management system and environment management \n",
    "> system that runs on Windows, macOS, and Linux. Conda quickly installs, runs, \n",
    "> and updates packages and their dependencies. Conda easily creates, saves, \n",
    "> loads, and switches between environments on your local computer. It was \n",
    "> created for Python programs but it can package and distribute software for \n",
    "> any language.\n",
    "\n",
    "For this course we recommend `miniconda` (https://docs.conda.io/en/latest/miniconda.html) for installing Python and other open source packages. 🐍"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c07624cb",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "<br />\n",
    "\n",
    "**Content**\n",
    "\n",
    "* [Why conda?](#motivation)\n",
    "* [Installation of conda](#install)\n",
    "* [Environments](#basics)\n",
    "* [Package installation with conda](#packages)\n",
    "* [Using pip and mamba](#pip)\n",
    "* [Create workshop environemnt](#workshop)\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df314216",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Why conda?  <a class=\"anchor\" id=\"motivation\"></a>\n",
    "\n",
    "🤔\n",
    "\n",
    "One benefit of the python world is its huge package ecosystem. Many great tools exist which can be installed by any scientist. One part of the work of python coders is to collect and arrange all tools that they plan to use. `Conda` is a solution which can help us to organize and manage this quickly developing ecosystem..\n",
    "\n",
    "**User managable installations** 💪\n",
    "\n",
    "With `conda`, we have the power to install packages without the need to ask a system admin to do it.\n",
    "\n",
    "**Flexible and free access to open source packages** 💪\n",
    "\n",
    "As it is well documented how packages should be hosted for conda, we have control and access to a large registry of open source packages.\n",
    "\n",
    "**Environment manager** 💪\n",
    "\n",
    "`conda` also manages software *environments* on user side allowing us to install different versions of packages."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d172554",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "**Dependency checks**\n",
    "\n",
    "Conda does *dependency checks* for new packages against *all* preinstalled packages at the same time in order to ensure that they are compatible. `conda` also installs all additional packages required by the target package.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf99ceb9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Concepts <a class=\"anchor\" id=\"concepts\"></a>\n",
    "\n",
    "- `miniconda`:\n",
    "\n",
    "Miniconda is a minimal version of `conda` as it only comes with the conda *installer*. The larger `anaconda` program also installs many additional libraries so it sums up to 3GB.\n",
    "\n",
    "- Channels 📺\n",
    "\n",
    "Channels are the names of the places where the packages are hosted. They correspond to an *URL* from where the packages are downloaded. A channel usually hosts more than one package but not manditorily. Channels distinguish between *standards* of how packages are tested and built or *licenses*. We highly recommend to use the channel `conda-forge` which is a community channel with high quality tests.\n",
    "\n",
    "- Environments 🖼️\n",
    "\n",
    "An environment can be thought of as a Python world completely encapsulated within itself. Several different environments can be created, for example to have different versions of the same software available on the computer at the same time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4316bd40",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "- Packages\n",
    "\n",
    "Packages are compressed tar balls of files (which the user does not have to deal with). They contain *precompiled* programs so that conda does not require compilers on the user system."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bce0c135",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "## Installation of conda <a class=\"anchor\" id=\"install\"></a>\n",
    "\n",
    "In Notebook on jupyterhub, `conda` is available within the *Python 3 unstable* kernel.\n",
    "\n",
    "If you work in a shell on DKRZ's HPC like mistral, you can activate a so called `module` which allows you to use a `conda` installation:\n",
    "\n",
    "`module load python3/unstable`\n",
    "\n",
    "Afterwards, you are able to use all conda commands.\n",
    "\n",
    "On local PC, the first thing to do is to download the installer for `miniconda` of the respective operating system for `Python 3.x`.\n",
    "\n",
    "- download miniconda installer from https://docs.conda.io/en/latest/miniconda.html\n",
    "- run the installer\n",
    "    + either `double-click on installer file`\n",
    "    + or run `bash <installer-name>` in a terminal window\n",
    "- follow the instructions"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ed98ca7",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> Run `conda` commands inside `bash` cells by specifying the magic `%%bash` in the first line of the cell\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21ae242a",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "On a local PC, as a first step, we need to update the `miniconda` installer with\n",
    "`conda update --all`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34293c26",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "#conda update --all -c conda-forge"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eaee5b82",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Creating an environment `helloworld`  <a class=\"anchor\" id=\"basics\"></a>\n",
    "\n",
    "As a starting point, we run the `conda info` command which gives us an overview over the configuration of conda. We can see what the `active environment` is and where it is installed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "144259ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df15f63f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "`conda env` shows you commands which are specific for managing *env*ironments. You can show all available commands with `cond env --help`.\n",
    "\n",
    "`conda env list` lists all environments while `conda list` lists all packages installed for the active environment. Let us compare these commands:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19a0572c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda env list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbb71236",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b66e31b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda env list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56710ab6",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "It is always useful to create one environment for each project one works for. This prevents the environments from version conflicts. So let us create a `helloworld` environment with `conda create -n helloworld`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c23a6360",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda create -n helloworld"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fffe1e95",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "We can activate the environment with `conda activate helloworld` or `source activate helloworld`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ecaf802",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "source activate helloworld\n",
    "conda info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9a832c0",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> `source activate` activates the environment for the shell you are working in. If you run this on jupyter notebooks, the shell closes with the end of the cell.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "410ab6cc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Install your first package <a class=\"anchor\" id=\"packages\"></a>\n",
    "\n",
    "We are moving towards the installation of our first package. You can search for packages by running `conda search <package_name>`. You can specify **channels** for the search by using the parameter `-c <channel_name>` or `--channel <channel_name>`.\n",
    "<h4 style=\"color:red\"> Exercise: Let's search for `xarray` in the channel `conda-forge`</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9139deff",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a523bce4",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda search -c conda-forge xarray"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6008d082",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "Conda always tries to find and install the most recent package which is by 2022/3 `2022.3.0`. Since our `helloworld` environment is empty, we will be able to install this version without problems. Otherwise, `conda` would check which version of `xarray` is compatible with our environment first. \n",
    "\n",
    "Software packages can be easily installed using the `conda install <package-name>` command. Without any additional parameter, the package is installed into the **activated** environment from the **default channel**. We an specify both with the *parameters* `-c <channel_name> -n <environment_name>`.\n",
    "\n",
    "<h4 style=\"color:red\"> Exercise: Install `xarray` from channel `conda-forge` into your `helloworld` environment</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aaa57fe1",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34aa86af",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda install -n helloworld -c conda-forge xarray fsspec\n",
    "\n",
    "#which is the same as\n",
    "#source activate helloworld\n",
    "#conda install -c conda-forge activate"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f13914c4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "You can also install packages **at the same time** with the creation of environments. `conda create` allows you to specify packages to *install* into the new environment similar to `conda install`:\n",
    "- Note that you can specify more than one package but rather a space-separated list of packages to install.\n",
    "- Note that you can specify a specific version of a package by just adding a suffix `<package_name>==<version>`\n",
    "\n",
    "This results in:\n",
    "\n",
    "`conda create -n <environment_name> -c <channel_name> <package_name> [<second_package_name>]`.\n",
    "\n",
    "<h4 style=\"color:red\"> Exercise: Install `xarray`  version 0.18.0 and the package `cartopy` from channel `conda-forge` into a new environment `helloworld2`</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a6c77de",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78e05ef1",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda create -n helloworld2 -c conda-forge xarray==0.18.0 cartopy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "412fbf00",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    },
    "tags": []
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> We recommend to set `conda-forge` as a default channel by: `conda config --add channels some-channel`\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c0b546e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## `pip`  and `mamba` <a class=\"anchor\" id=\"pip\"></a>\n",
    "\n",
    "`pip` is another package manager. Some software packages are not available for `conda` but for `pip` (another package installer https://pypi.org/project/pip/). First, we have to install pip with conda and then we can use pip to install the software in an activated environment.\n",
    "\n",
    "```bash\n",
    "conda install -n <environment-name> -c conda-forge pip\n",
    "pip install <package-name>\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62fbdd2b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "[Mamba](https://github.com/mamba-org/mamba) is a reimplementation of the conda package manager in C++.\n",
    "\n",
    "- parallel downloading\n",
    "- libsolv for much faster dependency solving\n",
    "- mainly implemented in C++ for maximum efficiency\n",
    "\n",
    "Mamba can be used in the same way as conda, it is compatible.\n",
    "\n",
    "`conda install mamba -c conda-forge`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2aa553e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Clean up and memory management <a class=\"anchor\" id=\"clean\"></a>\n",
    "\n",
    "Conda can easily exceed users disk space because \n",
    "\n",
    "- of its default **package cache**.\n",
    "- environments can become very large\n",
    "\n",
    "You can prevent memory problems when you keep your *conda* clean:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2b36ebf",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "**Remove environments**\n",
    "\n",
    "You can **uninstall** packages with `conda uninstall` and **remove** environemnts with `conda env remove -n <env_name>`.\n",
    "\n",
    "<h4 style=\"color:red\"> Exercise: Remove the environments `helloworld` and `helloworld2`</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b72ba45",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d0588b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda env remove -n helloworld2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ea56663b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda env list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e6fd3d7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda clean --all"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0eca63f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Create workshop environment <a class=\"anchor\" id=\"workshop\"></a>\n",
    "\n",
    "In order to simplify the installation of an entire environment, projects like this python workshop project collect all required packages in a file `environment.yml` in their git repositories. This file can be parsed by `conda env create` so that all packages from that file are installed in one command. The corresponding parameter is `conda env create -f <file_name>`.\n",
    "\n",
    "<h4 style=\"color:red\"> Exercise: Create the python workhop's environment</h4>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d008f604",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "#conda env create -f ../environment.yml"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "67536d8f",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "00a8823b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## Bonus: Create kernel for notebooks <a class=\"anchor\" id=\"kernel\"></a>\n",
    "\n",
    "If you want to create a **kernel** based on an environment, you have to install `ipykernel` into the environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "628540d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "conda create -n helloworld -c conda-forge ipykernel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a1dcd4c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "Afterwards, you have to\n",
    "\n",
    "- ensure that you have a *kernels* directory in your *home* directory\n",
    "- activate the environment\n",
    "- install a new kernel with *ipykernel*. You can specify names for the kernel."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8ac9e31b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "mkdir -p /home/k/k204210/kernels\n",
    "source activate /home/k/k204210/conda-envs/helloworld\n",
    "python -m ipykernel install --user --name pythoncourse_kernel --display-name=\"pythoncourse_kernel\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7e43557",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 unstable (using the module python3/unstable)",
   "language": "python",
   "name": "python3_unstable"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
