#!/usr/bin/env ksh

files=$(ls *.ipynb)

for f in $files
do
   echo "File: $f"
   jupyter nbconvert --clear-output $f
done

