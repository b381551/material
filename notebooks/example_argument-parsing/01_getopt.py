#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# source: https://www.python.org/dev/peps/pep-0263/
import sys, getopt
from typing import Dict

parsed_args: Dict={}
try:
    opts, args = getopt.getopt(sys.argv[1:],"hab:c:",["arg_b=","arg_c="])
except getopt.GetoptError:
    print(f"{sys.argv[0]} -a -b <argument> -c <argument>")
    sys.exit(1)
for opt, arg in opts:
    if opt == '-h':
        print(f"{sys.argv[0]} -a -b <argument> -c <argument>")
        sys.exit(0)
    elif opt == "-a":
        parsed_args[opt] = None
    elif opt in ("-b", "--arg_b"):
        parsed_args[opt] = arg
    elif opt in ("-c", "--arg_c"):
        parsed_args[opt] = arg

print(f"Parsed arguments: {str(parsed_args)}")
print(f"Omitted arguments: {str(args)}")