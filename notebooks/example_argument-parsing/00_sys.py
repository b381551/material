#!/usr/bin/env python3

import sys

print(f"You entered {len(sys.argv)} argument(s)")
for argument in sys.argv:
    print(f"{argument} (type: {type(argument)}")
