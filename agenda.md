
## Day 1 , 2022-03-21

| Starttime   | Endtime   | Title                                                                         | Content                                                                                                                                                                                                              |   Duration [min] |
|:------------|:----------|:------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------:|
| 09:00       | 09:10     | Welcome                                                                       | <ul> <li>A short welcome to the participants and speakers.</li><li>                                                                                                                                                  |               10 |
| 09:10       | 09:30     | Hardware for Python: Difference between a supercomputer and your mobile phone | <ul> <li> what is a node, cpu</li><li>  Historical development</li><li> Architecture</li><li>                                                                                                                        |               20 |
| 09:30       | 10:00     | What is Python – short intro                                                  | <ul> <li> Difference between programming language and Python</li><li> Assembler, compiler, interpreter</li><li>                                                                                                      |               30 |
| 10:00       | 11:00     | Introduction to Linux system and setting up work environment                  | <ul> <li> terminal</li><li> via WSL</li><li> command line and shell: (basics to transfer/run files and data in/out Mistral)</li><li> editor</li><li>                                                                 |               60 |
| 11:15       | 12:15     | Installation                                                                  | <ul> <li> conda</li><li> python</li><li> modules/environments</li><li> envkernel conda name=kernelname user $condaenv</li><li> pythonrelated package management</li><li> testing installation and libraries</li><li> |               60 |
| 12:15       | 13:15     | Break                                                                         | nan                                                                                                                                                                                                                  |               60 |
| 13:15       | 13:30     | Installation - Questions/Answers                                              | <ul> <li>                                                                                                                                                                                                            |               15 |
| 13:30       | 14:30     | Jupyter notebooks, connecting to Mistral                                      | <ul> <li> Jupyter lab</li><li> Jupyter hub</li><li> Jupyter lab</li><li>                                                                                                                                             |               60 |
| 14:30       | 15:00     | Syntax                                                                        | <ul> <li> casesensitive</li><li> print</li><li> indentation and code blocks</li><li> line continuation</li><li> comments</li><li>                                                                                    |               30 |
| 15:00       | 15:15     | Data types                                                                    | <ul> <li> boolean</li><li> integer</li><li> floatingpoint</li><li> complex</li><li> conversion between types</li><li> None</li><li>                                                                                  |               15 |
| 15:15       | 15:30     | Strings                                                                       | <ul> <li> formatting</li><li> string functions</li><li>                                                                                                                                                              |               15 |
## Day 2 , 2022-03-22

| Starttime   | Endtime   | Title                            | Content                                                                                                                                                        |   Duration [min] |
|:------------|:----------|:---------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------:|
| 09:00       | 09:15     | Warmup about content of Day 1    | <ul> <li>                                                                                                                                                      |               15 |
| 09:15       | 10:45     | List, Tupels, sets, arrays, ...  | <ul> <li> slices</li><li> dictionaries</li><li> intro to logic</li><li> copying objects, deep copy</li><li> mathematical operators and math functions</li><li> |               90 |
| 11:00       | 12:30     | Conditional statements, ...      | <ul> <li> Iteration</li><li> Loops</li><li> List comprehensions</li><li>                                                                                       |               90 |
| 12:30       | 13:30     | Break                            | nan                                                                                                                                                            |               60 |
| 13:30       | 15:45     | Functions, classes, scripts, ... | <ul> <li> Functions </li><li> Classes and methods</li><li> Command line argumentsfor python scripts</li><li> File I/O</li><li>                                 |              135 |
## Day 3 , 2022-03-23

| Starttime   | Endtime   | Title                                     | Content                                         |   Duration [min] |
|:------------|:----------|:------------------------------------------|:------------------------------------------------|-----------------:|
| 09:00       | 09:15     | Warmup about content of Day 2             | <ul> <li>                                       |               15 |
| 09:15       | 10:45     | Numpy                                     | <ul> <li>                                       |               90 |
| 11:00       | 12:00     | Xarray - read/write netCDF and GRIB files | <ul> <li>                                       |               60 |
| 12:00       | 13:00     | Break                                     | nan                                             |               60 |
| 13:00       | 13:30     | Exceptions/error handling                 | <ul> <li> try, except</li><li> logging</li><li> |               30 |
| 13:30       | 14:45     | Data analysis                             | <ul> <li> pythoncdo</li><li>                    |               75 |
| 14:45       | 15:45     | Python in action GLOBAGRIM                | <ul> <li>                                       |               60 |
## Day 4 , 2022-03-24

| Starttime   | Endtime   | Title                         | Content                                                         |   Duration [min] |
|:------------|:----------|:------------------------------|:----------------------------------------------------------------|-----------------:|
| 09:00       | 09:15     | Warmup about content of Day 3 | <ul> <li>                                                       |               15 |
| 09:15       | 11:15     | Visualization                 | <ul> <li> matplotlib</li><li> cartopy</li><li> seaborn</li><li> |              120 |
| 11:30       | 12:30     | Interesting packages          | <ul> <li>Overview of some recommendable packages</li><li>       |               60 |
| 12:30       | 13:30     | Break                         | nan                                                             |               60 |
| 13:30       | 15:30     | Exercise                      | <ul> <li>                                                       |              120 |
| 15:30       | 15:45     | Feedback                      | <ul> <li>                                                       |               15 |