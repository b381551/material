# Debugging: How to work with broken programs
What is the Problem? Why debugging? Everything works, right?

## No! Every real world program has errors. Amen

Two options:
- easy: the program crashes and shows error messages
- hard: the program seem to work properly, but the results are unexpected

### Example (easy)

Let's take an example of geocat, because they offer so many nice plots: [NCL_color_1.py](./scripts/NCL_color_1.py)
There should be some nice errors in it, too ;-)

Lessions:
- Understand your environment and its limitations
- Read error messages and warning. They are your best friends (most of the time)

An common example of a message hard to understand:
```python
print("a = {0}".format(55)
```
throws this:
```shell
SyntaxError: unexpected EOF while parsing
```
Keep it in mind - it will come back to you sooner or later, because missing a closing parenthesis is easy enough.

### Example (hard)

```python
import sys
def sum(a,b):
  return a+b

if __name__ == "__main__":
  # first some tests
  print("2 = {0}".format(sum(1,1)))
  print("5 = {0}".format(sum(1,4)))
  print("4 = {0}".format(sum(2,2)))
           
  # now some user given numbers
  a = sys.argv[1]
  b = sys.argv[2]
  print("a   = {0}".format(a))
  print("b   = {0}".format(b))
  print("sum = {0}".format(sum(a,b)))
```
### Example (harder)

What should be the result?
```python
0.3 == 0.1 + 0.2
True or False #???
```
It's quite obvious, isn't it? Try it!

Lessions:
- understand the type system of your language + their basic methods
- understand common difficulties of basic types (numbers, strings, ...)
- **Always-Available-Debugger: your mind + print**

More on floating point problems [here](https://floating-point-gui.de/) of as [pdf](https://www.phys.uconn.edu/~rozman/Courses/P2200_15F/downloads/floating-point-guide-2015-10-15.pdf)

# How to improve programs step-by-step: debuggability 
If `print()` is so useful, why not integrating it into the program? 

## Logging

```python
import sys
import logging
logging.basicConfig(level=logging.INFO)
# logging.basicConfig(filename='example.log',   # alternative with file
#   encoding='utf-8',
#   format='%(asctime)s :: %(levelname)s :: %(message)s',
#   level=logging.DEBUG)

def sum(a,b):
  logging.debug("started method sum()")
  logging.info("Got a = {0} of type {1}".format(a,type(a)))
  logging.info("Got b = {0} of type {1}".format(b,type(b)))
  return a+b

if __name__ == "__main__":
  a,b = sys.argv[1], sys.argv[2]
  print("{1}+{2}= {0}".format(sum(a,b)),a,b)
```

### basic log levels

- DEBUG: Detailed information, for diagnosing problems
- INFO: Confirm things are working as expected
- WARNING: Something unexpected happened, or indicative of some problem
- ERROR: More serious problem, the software is not able to perform some function
- CRITICAL: A serious error, the program itself may be unable to continue running


## Exceptions: try/except

Python has means to deal with non-intended behaviour called Exceptions. Using them will make your scripts easier to understand by others (and by yourself next week).

```python
try:
  filehandle = open(filename)
except IOError:
  print('File cannot be opened:', filename)
  exit()
```

![try/except overview](https://res.cloudinary.com/dyd911kmh/image/upload/v1633675298/python-try-except-else-finally_doblhm.png "try/except workflow")

- Think about what could possibly go wrong
- Think about which requirements must be met for your code work


Exercise:
- come up with exceptions handling for more functions: div, sqrt, sending email, open an webpage, saving a file, filling an online form, ...

## python's own debugger: pdb/pudb

... this needs an interactive session ...

- [official docu](https://docs.python.org/3.10/library/pdb.html)
- [list of python debugging tools](https://wiki.python.org/moin/PythonDebuggingTools)
