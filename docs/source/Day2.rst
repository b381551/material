Day2
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
  
   python_core_language_part1.ipynb
   python_core_language_part2.ipynb
   python_core_language_part3.ipynb
