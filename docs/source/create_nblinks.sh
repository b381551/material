#!/bin/sh
find ../../notebooks/ -name "*.ipynb" ! -name "*checkpoint*" | xargs -i ln -s {}
