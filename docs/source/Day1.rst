Day1
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   INTRO.md
   introduction_linux.md
   conda_introduction.ipynb
   git_intro.ipynb
   jupyter.ipynb
