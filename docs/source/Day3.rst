Day3
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   file_input_output.ipynb
   numpy_intro.ipynb
   xarray_introduction-part1.ipynb
   xarray_introduction-part2.ipynb
   debugging.ipynb
   cdo_introduction.ipynb
