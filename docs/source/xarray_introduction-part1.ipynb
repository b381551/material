{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9eb1ee0a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "# Xarray - Introduction\n",
    "\n",
    "![xarray logo](../images/xarray_logo.png)\n",
    "\n",
    "https://xarray.pydata.org/en/stable/index.html\n",
    "\n",
    "⚡️ **xarray** is a python package which allows us to handle multi-dimensional datasets in a simple way. It provides a huge set of functions for advanced analytics and visualization. It is part of higher level package ecosystems like [Pangeo](https://pangeo.io/).\n",
    "\n",
    "⭐️ **xarray**'s underlying data model is borrowed from the data format [NetCDF](http://www.unidata.ucar.edu/software/netcdf). This data format in combination with the [Climate and Forecast conventions](https://cfconventions.org/) is the standard for the climate science community. A large part of DKRZ's data is available in netCDF. Therefore, `xarray` allows fast and intuitive data analysis on this kind of data.\n",
    "\n",
    "💥 **xarray** data structure deals with scientific data by using labels, attributes, dimensions and coordinates, and extend the capabilities of **NumPy** and **pandas**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4eb67190",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Agenda\n",
    "\n",
    "* [Xarray's Data Model](#datamodel)\n",
    "* [DataArrays](#DataArray)\n",
    "* [Dimensions](#Dimensions)\n",
    "* [Coordinates](#Coordinates)\n",
    "* [Variable attributes](#Variableattributes)\n",
    "* [Datasets](#Datasets)\n",
    "* [Open and read files](#openandread)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32d50a39",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Requirements\n",
    "\n",
    "* [numpy](numpy_intro.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3db3bf2",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"datamodel\"></a>\n",
    "\n",
    "## Overview: **Xarray's** data model\n",
    "\n",
    "A **data model**  🗃️ describes how the package organizes elements of data and standardizes how they relate to one another. On code level, a graph of a data model shows the interconnections of classes, types and methods. **Xarray's** data model consists of the classes *Dataset*, *DataArray*, *Dimension*, *Coordinate* and *attributes*.\n",
    "\n",
    "📎 Dataset ( ≈ file ): \n",
    "\n",
    "    Dict-like collection of DataArray objects with aligned dimensions. Similar use of variables, dimensions, coordinates, and attributes like for DataArray. You can see an xarray Dataset as a netCDF file like object. Has no data itself but only pointers to DataArrays\n",
    "\n",
    "💾  DataArray ( = variable in the file ): \n",
    "\n",
    "    N-dimensional array with dimensions. The objects add dimension names, coordinates, and attibutes to the underlying data structure (numpy and dask arrays).\n",
    " \n",
    "↔️  Dimensions: \n",
    "\n",
    "    Named dimension axes, if missing the dimension names are dim_0, dim_1, ...\n",
    "\n",
    "\n",
    "🌎 Coordinates: \n",
    "\n",
    "    An array which labels a dimension. Two types are defined a) dimension coordinates - 1-dimensional coordinate array assigned to the DataArray with a name and dimension name. b) Non-dimensional coordinate - a coordinate array assigned to DataArray with the name assigned to the coordinates and not to the dimensions.\n",
    "\n",
    "<br />\n",
    "\n",
    "![xarray_core_data_structure.png](../images/xarray_core_data_structure.png)\n",
    "From https://xarray-contrib.github.io/xarray-tutorial/online-tutorial-series/01_xarray_fundamentals.html\n",
    "\n",
    "<br/>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b67f3846",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Importing modules\n",
    "\n",
    "In this notebook, the Python libraries Numpy, Pandas, and cfgrib are needed for the examples. \n",
    "\n",
    "```python\n",
    "import xarray as xr\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import cfgrib\n",
    "\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2ef94e3",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "If you work with jupyter lite, *before* importing the packages do:\n",
    "\n",
    "```python\n",
    "import micropip\n",
    "await micropip.install(['xarray','cfgrib'])\n",
    "```\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ecfd1226",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import xarray as xr\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import cfgrib"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7afb1c3f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"DataArray\"></a>\n",
    "\n",
    "## Xarray's DataArray compared with numpy's data array\n",
    "\n",
    "🔄 As a start, we compare the `numpy` array with an `xarray`'s **DataArray** type. You can directly convert a `numpy` array into an `xarray` **DataArray** type by using it as input for `xarray`'s function `DataArray`. We use the data from the file `pr.dat` by loading it with `numpy`.\n",
    "\n",
    "```python\n",
    "pr_data = np.loadtxt('pr.dat', usecols=(1,2,3), skiprows=1)\n",
    "pr_data_xr = xr.DataArray(pr_data)\n",
    "pr_data_xr\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2961b430",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "In Jupyterlite, you can do:\n",
    "\n",
    "```python\n",
    "from js import fetch\n",
    "res = await fetch('https://swift.dkrz.de/v1/dkrz_0b2a0dcc-1430-4a8a-9f25-a6cb8924d92b/python_workshop/pr.dat')\n",
    "text = await res.text()\n",
    "\n",
    "from io import StringIO\n",
    "f = StringIO(text)\n",
    "```\n",
    "\n",
    "where f can be opened by numpy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09b35e35",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data = np.loadtxt('../data/pr.dat', usecols=(1,2,3), skiprows=1)\n",
    "pr_data_xr = xr.DataArray(pr_data)\n",
    "print(pr_data_xr)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d1bbaa0",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "`pr_data_xr` has got more structure and descriptive information than `pr_data`. In contrast to the `numpy` data array, the `Xarray's` DataArray can separate the variable of interest, `pr`, as a *data variable* from *coordinate* variables. In summary, it contains:\n",
    "\n",
    "\n",
    "- ↔️ **dimensions** with names              (`pr_data_xr.dims`)\n",
    "- 🌎 **coordinates** pointing to variables  (`pr_data_xr.coords`)\n",
    "- 🎨  and **attributes**                     (`pr_data_xr.attrs`)\n",
    "\n",
    "Not only `xarray` but other software tools require and use the **labeld geospatial** information from coordinates, for example for\n",
    "\n",
    "- 🖼️ **plotting**: mapping of data on a real world grid point\n",
    "- 🖩 **analysis**: implemented routines for e.g. area *weighted* means can be run"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d7f0b3d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "This information is not correctly parsed from the input numpy array per default when executing `xr.DataArray()`. But we know them so we need to configure the call `xr.DataArray()` via the function parameters (arguments + keyword arguments):\n",
    "\n",
    "```python\n",
    "xr.DataArray(data,\n",
    "             coords=,\n",
    "             dims=,\n",
    "             name=,\n",
    "             attrs\n",
    "            )\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2da8f208",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> When working with <b>xarray</b>, the arguments and keyword arguments for a function are <i>in general</i> very usefull and important!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ba3f02a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Create an DataArray from numpy data and additional labels\n",
    "\n",
    "Let's define a clear structure for the `xarray.DataArray()` for the numpy data first:\n",
    "\n",
    "1. The actual **data** for the data variable is in the first column of the `numpy` array.\n",
    "2. The **coords** are the second and third column of the `numpy` array. They have the same dimension as the data array.\n",
    "3. We have one dimension (**dims**) which refers to the *station*. It is an index which runs from 0 to the length of the a column minus 1.\n",
    "4. The **name** of the data variable is *Precipitation*.\n",
    "5. In the **attrs**, we can store variable attributes like *units*.\n",
    "\n",
    "Let's bring that into context with `xr.DataArray()`:\n",
    "```python\n",
    "pr_data_xr = xr.DataArray(pr_data[:,0],\n",
    "                          coords={\"lon\":(\"Station\",pr_data[:,1]),\n",
    "                                  \"lat\":(\"Station\",pr_data[:,2])},\n",
    "                          dims=[\"Station\"],\n",
    "                          name=\"Precipitation\",\n",
    "                          attrs={\"units\":\"mm\",\n",
    "                                \"coords\":\"lon lat\"})\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7fd886b7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr = xr.DataArray(pr_data[:,0],\n",
    "                          coords={\"lon\":(\"Station\",pr_data[:,1]),\n",
    "                                  \"lat\":(\"Station\",pr_data[:,2])},\n",
    "                          dims=[\"Station\"],\n",
    "                          name=\"Precipitation\",\n",
    "                          attrs={\"units\":\"mm\",\n",
    "                                \"coords\":\"lon lat\"})\n",
    "pr_data_xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a664fea",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "print(\"Variable Name: \",pr_data_xr.name)\n",
    "print(\"Dimensions: \",pr_data_xr.dims)\n",
    "print(\"Coordinates: \",pr_data_xr.coords)\n",
    "print(\"Sizes: \",pr_data_xr.sizes)\n",
    "print(\"Attribute: \",pr_data_xr.attrs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c673a0c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"Dimensions\"></a>\n",
    "\n",
    "## Dimensions\n",
    "\n",
    "↔️ Dimensions are **indices** covering an interval of the length of the dimension.\n",
    "\n",
    "In our example, we only have one dimension where each index refers to one **station**. However, if create a quick plot of the data with the DataArray variable's `.plot()` function, we only get a one dimensional view:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9e5f4dc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49374404",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Create a two dimensional georeferenced plot 🖼️\n",
    "\n",
    "Our goal for this session now is to reorganize the data so that `.plot()` returns a meshed grid plot.\n",
    "For that, we create a less condensed **two-dimensional** DataArray (with a lot of `NaN` values). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10c0a0b0",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<br />\n",
    "\n",
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "1. Create a two dimensional numpy with the size `len(pr_data)` x `len(pr_data)`\n",
    "\n",
    "1. Assign `NaN` values to the entire array\n",
    "\n",
    "1. On the diagonal of the quadratic array, insert the values of `pr_data`\n",
    "\n",
    "1. Show the new data frame\n",
    "\n",
    "<br />\n",
    "\n",
    "You will need:\n",
    "\n",
    "- `np.empty()`\n",
    "- `np.Nan`\n",
    "- `for` loop\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eabbbcc4",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec5fed04",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_2d = np.empty((len(pr_data), len(pr_data)))\n",
    "pr_data_2d[:] = np.NaN\n",
    "for i in range(len(pr_data)):\n",
    "    pr_data_2d[i,i]=pr_data[i,0]    \n",
    "pr_data_2d"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b233fe2b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Let's pass this DataArray to **Xarray**.\n",
    "\n",
    "<br />\n",
    "\n",
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "1. Reset the variable `pr_data_xr` with a `xr.DataArray()` but use `pr_data_2d` as input.\n",
    "\n",
    "    1.1. Set a correct configuration for the parameters of the function.\n",
    "\n",
    "2. Plot again\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb89757b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "500d5b66",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr = xr.DataArray(pr_data_2d,\n",
    "                          coords={\"lon\":(\"i\",pr_data[:,1]),\n",
    "                                  \"lat\":(\"j\",pr_data[:,2])},\n",
    "                          dims=[\"i\", \"j\"],\n",
    "                          name=\"Precipitation\",\n",
    "                          attrs={\"units\":\"mm\",\n",
    "                                \"coordinates\":\"lon lat\"})\n",
    "pr_data_xr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e9ace42",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We plot the two dimension xr:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a52e2928",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1a391fc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"Coordinates\"></a>\n",
    "\n",
    "## Coordinates\n",
    "\n",
    "🌎 The plot only uses the indices of the dimensions for the x and y axes of the plot. This is because the **coordinates** `lat` and `lon` are not interpreted as **index coordinates**. `Xarray` will interpete coordinates as **index coordinates** only if the name of the coordinate is the same as the name of the dimension. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d01aaae",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<br />\n",
    "\n",
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "1. Reset the variable `pr_data_xr` with a `xr.DataArray()` but rename `coords` or `dims` so that they are equal.\n",
    "2. Plot again\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17d63383",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr = xr.DataArray(pr_data_2d,\n",
    "                          coords={\"lon\":(\"lon\",pr_data[:,1]),\n",
    "                                  \"lat\":(\"lat\",pr_data[:,2])},\n",
    "                          dims=[\"lon\", \"lat\"],\n",
    "                          name=\"Precipitation\",\n",
    "                          attrs={\"units\":\"mm\",\n",
    "                                \"coordinates\":\"lon lat\"})\n",
    "pr_data_xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e63fee64",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cf7c78e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "You will receive a\n",
    "\n",
    "```Python\n",
    "ValueError: The input coordinate is not sorted in increasing order along axis 0. Consider calling the `sortby` method on the input DataArray.\n",
    "```\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "    <b>Note:</b> When running into errors with <b>xarray</b>, the output will be very helpful and guiding. Be not afraid of making mistakes!\n",
    "</div>\n",
    "\n",
    "So let's use `sortby`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e764fd9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "print(pr_data_xr.lat.values)\n",
    "pr_data_xr=pr_data_xr.sortby([\"lon\",\"lat\"])\n",
    "print(pr_data_xr.lat.values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dcf062d1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "a=pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd22f2d5",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Wrong dimension size? There are sevaral ways to repair this. One is to use `xarray`'s transpose function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "208a5780",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr=pr_data_xr.transpose(\"lat\",\"lon\")\n",
    "pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0dedc464",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "We created a plot which gives us an idea of for which places the data is valid with only few commands based on `xarray`.\n",
    "\n",
    "- The *boundaries* of the grid points are artificial. They are not specified but only rendered by the plot function.\n",
    "- In the next sessions we will learn a more sophisticated plotting including e.g. *coastlines*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ef6b732f",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "import cartopy.crs as ccrs\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "proj=ccrs.PlateCarree()\n",
    "ax = plt.axes(projection=proj)\n",
    "ax.set_extent([-120, -80, 20, 60], proj)\n",
    "ax.stock_img()\n",
    "ax.coastlines()\n",
    "\n",
    "pr_data_xr.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee60c549",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"Variableattributes\"></a>\n",
    "\n",
    "## Variable attributes\n",
    "\n",
    "<br /> \n",
    "\n",
    "🎨 You can easily set an attribute, for instance the attribute _name_ :\n",
    "\n",
    "```python\n",
    "pr_data_xr.name = 'precip'\n",
    "```\n",
    "\n",
    "<br />\n",
    "\n",
    "Variables in Earth Science commonly have attributes like **standard_name**, **long_name** or **units** which can be added via the _attrs_ attribute to the DataArray. \n",
    "\n",
    "Add the units attribute to the DataArray _da_ :\n",
    "\n",
    "```python\n",
    "pr_data_xr.attrs['units'] = 'mm'\n",
    "```\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a14e2ed",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "1. Add the variable attribute units as shown above\n",
    "1. Add the variable long_name (as you like ;))\n",
    "1. Change the long_name\n",
    "1. Print all attributes\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4d09f2c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "pr_data_xr.name = 'temp'\n",
    "pr_data_xr.attrs['units'] = 'K'\n",
    "pr_data_xr.attrs['long_name'] = 'temperature at 2m'\n",
    "print(pr_data_xr.attrs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b7071fa",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"Datasets\"></a>\n",
    "\n",
    "## Datasets\n",
    "\n",
    "📎 Xarray's function `open_dataset` can be used to open and read the content of a file. It supports various formats, such as **netcdf, grib, zarr**, etc. (default: netcdf4). The file content will be  stored in the Xarray Dataset structure.\n",
    "\n",
    "Example:\n",
    "\n",
    "In the data directory of the course material, we use the file _tsurf.nc_ to demonstrate Xarray's file handling.\n",
    "\n",
    "```python\n",
    "ds = xr.open_dataset('../data/tsurf.nc')\n",
    "\n",
    "ds.info()\n",
    "```\n",
    "\n",
    "Result:\n",
    "\n",
    "```python\n",
    "xarray.Dataset {\n",
    "dimensions:\n",
    "\tlat = 96 ;\n",
    "\tlon = 192 ;\n",
    "\ttime = 40 ;\n",
    "\n",
    "variables:\n",
    "\tdatetime64[ns] time(time) ;\n",
    "\t\ttime:standard_name = time ;\n",
    "\t\ttime:axis = T ;\n",
    "\tfloat64 lon(lon) ;\n",
    "\t\tlon:standard_name = longitude ;\n",
    "\t\tlon:long_name = longitude ;\n",
    "\t\tlon:units = degrees_east ;\n",
    "\t\tlon:axis = X ;\n",
    "\tfloat64 lat(lat) ;\n",
    "\t\tlat:standard_name = latitude ;\n",
    "\t\tlat:long_name = latitude ;\n",
    "\t\tlat:units = degrees_north ;\n",
    "\t\tlat:axis = Y ;\n",
    "\tfloat32 tsurf(time, lat, lon) ;\n",
    "\t\ttsurf:long_name = surface temperature ;\n",
    "\t\ttsurf:units = K ;\n",
    "\t\ttsurf:code = 169 ;\n",
    "\t\ttsurf:table = 128 ;\n",
    "\n",
    "// global attributes:\n",
    "\t:CDI = Climate Data Interface version 1.9.6 (http://mpimet.mpg.de/cdi) ;\n",
    "\t:Conventions = CF-1.6 ;\n",
    "\t:history = Thu Oct 10 16:08:50 2019: cdo selname,tsurf rectilinear_grid_2D.nc tsurf.nc ;\n",
    "\t:CDO = Climate Data Operators version 1.9.6 (http://mpimet.mpg.de/cdo) ;\n",
    "}\n",
    "```\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74c0d076",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "ds = xr.open_dataset('../data/tsurf.nc')\n",
    "\n",
    "ds.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5947abe",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<br />\n",
    "\n",
    "### Show variable names and coordinates\n",
    "\n",
    "🌎 It is always good to have a closer look at the data, and this can be done very easily using the attributes explained above.\n",
    "\n",
    "Show the coordinates stored in file:\n",
    "\n",
    "```python\n",
    "coords = ds.coords\n",
    "```\n",
    "\n",
    "Result:\n",
    "\n",
    "```python\n",
    " Coordinates:\n",
    "  * time     (time) datetime64[ns] 2001-01-01 ... 2001-01-10T18:00:00\n",
    "  * lon      (lon) float64 -180.0 -178.1 -176.2 -174.4 ... 174.4 176.2 178.1\n",
    "  * lat      (lat) float64 88.57 86.72 84.86 83.0 ... -83.0 -84.86 -86.72 -88.57\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4752a681",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "List the variables stored in the file:\n",
    "\n",
    "```python\n",
    "variables = ds.variables\n",
    "\n",
    "```\n",
    "\n",
    "Here we can see the time displayed in a readable way, because Xarray use the datetime64 module under the hood. Also the variable and coordinate attributes are displayed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "708978ba",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "Read the file and try the above commands.\n",
    "\n",
    "<br />"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95c7193f",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce3db0e5",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "coords = ds.coords\n",
    "variables = ds.variables\n",
    "\n",
    "coords\n",
    "variables"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc08232b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "tsurf = ds.tsurf"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f890a561",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Dimensions, shape and size\n",
    "\n",
    "To get more informations about the dimension, shape and size of a **Dataset**, we can use the appropriate attributes.\n",
    "\n",
    "```python\n",
    "dims  = ds.dims\n",
    "shape = tsurf.shape\n",
    "size  = tsurf.size\n",
    "rank  = len(shape)\n",
    "\n",
    "print('dimensions: ', dims)\n",
    "print('shape:      ', shape)\n",
    "print('size:       ', size)\n",
    "print('rank:       ', rank)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e9d8c87",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "86a19298",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "dims  = ds.dims\n",
    "shape = tsurf.shape\n",
    "size  = tsurf.size\n",
    "rank  = len(shape)\n",
    "\n",
    "print('dimensions: ', dims)\n",
    "print('shape:      ', shape)\n",
    "print('size:       ', size)\n",
    "print('rank:       ', rank)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "447e07b6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a class=\"anchor\" id=\"openandread\"></a>\n",
    "\n",
    "## Open and read files of different formats\n",
    "\n",
    " 💾 💽 📀 \n",
    "Xarray needs an _engine_ to read another file format. Here, we demonstrate how to read a GRIB file using the **cfgrib**  _engine_ from the additional library __cfgrib__ (don't forget to import it).\n",
    "\n",
    "```python\n",
    "import cfgrib\n",
    "\n",
    "ds2 = xr.open_dataset('../data/MET9_IR108_cosmode_0909210000.grb2',\n",
    "                      engine='cfgrib')\n",
    "\n",
    "variables2 = ds2.variables\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bafc0c41",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "Read the GRIB file yourself.\n",
    "\n",
    "<br />"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e48e9bb",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15cd5b0c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "ds2 = xr.open_dataset('../data/MET9_IR108_cosmode_0909210000.grb2',\n",
    "                      engine='cfgrib')\n",
    "\n",
    "variables2 = ds2.variables\n",
    "variables2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27e786d3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<br>\n",
    "\n",
    "### Open multiple files\n",
    "\n",
    "📎📎📎 In the course directory **data** there are 3 files _precip_day01.nc, precip_day02.nc, and precip_day03.nc_, each containing the data of one day in 6 hour intervals. \n",
    "\n",
    "**Xarray** provides the function `open_mfdataset` to read multiple files in one step as a single dataset. Before you can use `open_mfdataset` make sure that the Python module **dask** is installed in your environment.\n",
    "\n",
    "<br>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dc7ce6ea",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "path = '../data/precip_day*.nc'\n",
    "\n",
    "dsm = xr.open_mfdataset(path)\n",
    "\n",
    "dsm"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56770cfc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "One reason why `xarray` is very fast with multiple files is that it does not **load** the data when the files are opened. This is possible by using an underlying library named `dask`. You can recognize that by checking for the `precip` variable in `dsm`.\n",
    "\n",
    "```python\n",
    "dsm.precip[1,4,5]\n",
    "```\n",
    "will not show you an exact value but only a description of what this output will be. You would have to load the data into memory first for accessing one specific point of the array. This is most often not necessary for your workflow.\n",
    "\n",
    "The entire array can be loaded into memory by `dsm.precip.load()`. You can also do: \n",
    "```python\n",
    "dsm.precip.values[1,4,5]\n",
    "```\n",
    "\n",
    "➡️ While data is not in loaded, you can work on files that are *larger than memory*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "73d00340",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "dsm.precip[1,4,5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74e41403",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "dsm.precip.load()\n",
    "dsm.precip[1,4,5]\n",
    "# is the same as\n",
    "dsm.precip.values[1,4,5]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2ece290",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "source": [
    "The [open_mfdataset](http://xarray.pydata.org/en/stable/generated/xarray.open_mfdataset.html?highlight=open_mfdataset) function is very powerful. It contains over **10 arguments** which allow users to configure how the files are combined:\n",
    "\n",
    "- On what dimension should the data be concatted\n",
    "- How strict should tests ensure that the data can be concatted\n",
    "- What are coordinates, what are data variables"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e51ac43c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Dataset attributes\n",
    "\n",
    "🎨 Dataset attributes and variable attributes are important for understanding what the data represents not only for human but also the machine. Therefore, it is important that they are available and have a standard format. In addition to the attributes of a `DataArray`, there also **global** or dataset attributes.\n",
    "\n",
    "```python\n",
    "tas_hr=xr.open_dataset(\"/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/tas_Amon_MPI-ESM1-2-HR_ssp370_r1i1p1f1_gn_201501-201912.nc\")\n",
    "tas_hr_atts = list(tas_hr.attrs)\n",
    "global_atts = list(tas_hr.attrs)\n",
    "print(global_atts)\n",
    "```\n",
    "\n",
    "Assumed that we know the variable and attribute names, we can get their content immediately.\n",
    "\n",
    "```python\n",
    "units = tas_hr.tas.units\n",
    "\n",
    "print('units:     ', units)\n",
    "```\n",
    "\n",
    "<br />\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6498f481",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "#on levante:\n",
    "#tas_hr=xr.open_dataset(\"/work/ik1017/CMIP6/data/CMIP6/ScenarioMIP/DKRZ/MPI-ESM1-2-HR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/tas_Amon_MPI-ESM1-2-HR_ssp370_r1i1p1f1_gn_201501-201912.nc\")\n",
    "#from swift:\n",
    "!wget https://swift.dkrz.de/v1/dkrz_0b2a0dcc-1430-4a8a-9f25-a6cb8924d92b/python_workshop/tas_Amon_MPI-ESM1-2-HR_ssp370_r1i1p1f1_gn_201501-201912.nc\n",
    "tas_hr=xr.open_dataset(\"tas_Amon_MPI-ESM1-2-HR_ssp370_r1i1p1f1_gn_201501-201912.nc\")\n",
    "global_atts = list(tas_hr.attrs)\n",
    "print(global_atts)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9cf32f46",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "tas_hr.attrs[\"source\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29a493bc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<h2 style=\"color:red\"> Exercise </h2>\n",
    "\n",
    "List the attributes of the variable _tas_ and print their content."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "83807275",
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aa738871",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "attributes = list(tas_hr.attrs)\n",
    "print(attributes)\n",
    "print(tas_hr.long_name)\n",
    "print(tas_hr.units)\n",
    "print(tas_hr.code)\n",
    "print(tas_hr.table)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f91b22ce",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "taucenv",
   "language": "python",
   "name": "taucenv"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
