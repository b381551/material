# Description

Germany's National Meteorological Service, the [Deutscher Wetterdienst](https://www.dwd.de/EN/Home/home_node.html) (DWD), provides a selection of its latest forecast simulation for free. The data can be accessed via https://opendata.dwd.de/.  For this exercise we are interested in the latest forecast run for the EU-nested ICON region and focus on the 2 meter temperature.

In this exercise you will:

1. Handle the download of the data
2. Generate a textual represantation of the latest temperature forecast

## 01 - Download
Download manually the first timestep of the 2m temperature. You can download it via your [web browser](https://opendata.dwd.de/weather/nwp/icon-eu/grib/00/t_2m/) or do it via cmd (e.g. with `wget` or `curl`). The first timestep is called:

`icon-eu_europe_regular-lat-lon_single-level_2021102000_000_T_2M.grib2.bz2` 

You have to adjust the date to the current or last day. You can make yourself familiar with the file content using `cdo` or `ncview`.

Downloading the files by hand becomes quickly cumbersome, you want to automatise this process. You need to pay attention to the fact, that each timestep resides in a single file and are compressed. Therefore there are three steps you script must achieve:

1. Download all 2m temperature files from the server to a local directory.
2. Decompress all the files
3. Merge all timesteps

### Hints

- You can use the `urllib` package to download the timesteps.
- You can use the `bz2` package do decompress the files.
- You can use the `cdo` package to merge the files. Pay attention to not mix the order of the single timesteps.

## 02 - Textual representation

Your script shall be executed with two parameters: longitude and latitude. Based on this input your script shall write on the terminal, what the current and expected weather could look like.

## Extra
You can add an additional variable, which shall be considered in all three steps (e.g. total precipitation).
