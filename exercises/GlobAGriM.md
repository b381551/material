# GloBaGriM Exercise

## 1. Instability
Initialize the model with the second experiment and let it run with the following parameters: 
- Number of west-east circles of longitude: 144
- Number of north-south circles of latitude: 72
- Total integration time in hours: 0.5
- Integration time step in seconds: 30
Keep the default option for the other paramtets and name your output ``unstable.nc``.


## 2. Visualization
You may have noticed that the model produces unrealistic values. The model becomes unstable. This happens when either the temporal resolution is too coarse or the spatial resolution is too fine.

Visualize the moment and place where the model becomes unstable with Matplotlib or Cartopy.

## 3. Procuce a Stable Model Run
Change either the integration time step ``DT`` or the spatial resolution (``NJ`` or ``NK``) in order to achieve a stable model run. Let the model run for at least two hours. Compare your results with the unstable model run during various time steps.

Hint: Satisfying [The Courant–Friedrichs–Lewy condition](https://en.wikipedia.org/wiki/Courant%E2%80%93Friedrichs%E2%80%93Lewy_condition) helps you to keep the modle stable.

## 4. Additional Experiment
Add another experiment to the model. You can be very creative. ;-)
