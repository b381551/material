# Climate Change: Global warming and the CO2 emissions

Based on a paper by Dirk Notz  https://doi.org/10.1126/science.aag2345

lets try to validate the correlation with the current data from CMIP6

## collect the data

select at leasts three models from CMIP6 which provide the data

## post-processing

Data is 1d,2d or 3d. Compute a global time-series for 
- northern hemosphere se ice extend
- global CO2 emissions

## visualization

- Prepare a XY-plot for show the possible correlation of the above variables
- compare different CMIP6 models


## hints
- plotting: matplotlib
- concentrate on sea ice minimum (mean September value)

## extra
- How does the correlation look like, if you focus on the min September value of sea-ice-extend?
- What about southern sea ice extend? Why does this looks different?
