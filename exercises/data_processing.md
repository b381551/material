# Exercise: CMIP6 - Plot Time Series of annual means

<br>

In this task, the global annual means of the CMIP6 variable `tas` are to be calculated and plotted.

Since one time series alone would be quite boring we want to generate a total of 4 time series of <br>
the scenarios 

    SSP 1-2.6
    SSP 2-4.5
    SSP 3-7.0
    SSP 5-8.5

For more information about the scenarios see

https://www.dkrz.de/en/communication/climate-simulations/cmip6-en/the-ssp-scenarios

The data are stored in several separate files and are to be merged with CDO and then their field mean average calculated. 

### In the exercise you will:

1. Learn to use the netCDF input files
1. Process the data with CDO or xarray
1. Visualize the processed data
1. Create a plot for a poster using seaborn

An example plot of the scenarios of some more ensembles and their mean values can be found on the DKRZ <br>
page for CMIP6

https://www.dkrz.de/de/kommunikation/klimasimulationen/cmip6-de/cmip6-aktivitaeten-am-dkrz-ueberblick

### Input Data

**Historical**:
	/pool/data/CMIP6/data/CMIP/MPI-M/MPI-ESM1-2-LR/historical/r1i1p1f1/Amon/tas/gn/v20190710/

**Scenario SSP 1-2.6**:
    /pool/data/CMIP6/data/ScenarioMIP/MPI-M/MPI-ESM1-2-LR/ssp126/r1i1p1f1/Amon/tas/gn/v20190710/
 
**Scenario SSP 2-4.5**:
    /pool/data/CMIP6/data/ScenarioMIP/MPI-M/MPI-ESM1-2-LR/ssp245/r1i1p1f1/Amon/tas/gn/v20190710/

**Scenario SSP 3-7.0**:
    /pool/data/CMIP6/data/ScenarioMIP/MPI-M/MPI-ESM1-2-LR/ssp370/r1i1p1f1/Amon/tas/gn/v20190710/

**Scenario SSP 5-8.5**:
    /pool/data/CMIP6/data/ScenarioMIP/MPI-M/MPI-ESM1-2-LR/ssp585/r1i1p1f1/Amon/tas/gn/v20190710/

### CDO hints:

1. Merge files along time (mergetime)
1. Compute the annual mean (yearmean)
1. Compute the field means (fldmean)
1. Take care of the shapes
1. Important: clean temporary files

### Xarray hints:

1. Open multiple files at once
1. Compute the weights (http://xarray.pydata.org/en/stable/examples/area_weighted_temperature.html)
1. Compute the annual mean (resample)
1. Compute the field means (mean)
