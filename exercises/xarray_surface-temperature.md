### An Xarray session on the file `../data/tsurf.nc`

Use `xarray` to answer the following questions:

1. What is the `long name`, `units` and `frequency` of the data variable?
2. Where do *summer days* occur? A *summer day* shall be defined as a day when `tsurf` exceeds 25degC. Calculate and plot *summer days*.
    - Where does the maximum of summer days occurs and what is it value?
3. What is the average daily maximum surface temperature at the grid point of hamburg?
4. Assume that it is essential for a very good vintage / grape harvest / "Weinlese" that `tsurf` has a diurnal value range from 15degC to 25degC. Where would you grow wine?